import React, { Component } from "react";
import { Grid, Col, Row } from "react-flexbox-grid";
import { Select, Table, Input, H1 } from "./styles";

class Exchange extends Component {
  constructor(props) {
    super(props);
    this.state = {
      amountInTenge: 0,
      initialCurrency: props.currencies[0].id,
      resultCurrency: props.currencies[1].id
    };
  }
  getInitialCurrency = () => {
    const { initialCurrency } = this.state;
    const { currencies } = this.props;
    const currency = currencies.find(
      curr => curr.id === parseInt(initialCurrency, 10)
    );
    return currency;
  };

  getResultCurrency = () => {
    const { resultCurrency } = this.state;
    const { currencies } = this.props;
    const currency = currencies.find(
      curr => curr.id === parseInt(resultCurrency, 10)
    );

    return currency;
  };

  handleAmountChange = currency => e => {
    const amount = parseInt(e.target.value, 10);
    this.setState({ amountInTenge: amount * currency });
  };

  render() {
    const { amountInTenge, initialCurrency, resultCurrency } = this.state;
    const { currencies } = this.props;

    return (
      <Grid>
        <Col xs={12} style={{ margin: "2rem 0 0" }}>
          <H1>Калькулятор</H1>
          <Row style={{ alignItems: "center" }}>
            <Col xs={5}>
              <Input
                name="amountInTenge"
                disabled={!initialCurrency && initialCurrency !== 0}
                value={Math.round(
                  !amountInTenge && amountInTenge !== 0
                    ? ""
                    : amountInTenge / this.getInitialCurrency().price
                )}
                onChange={this.handleAmountChange(
                  this.getInitialCurrency().price
                )}
              />
            </Col>
            <Col xs={2} />
            <Col xs={5}>
              <Select
                name="initialCurrency"
                value={initialCurrency}
                // onChange={this.handleChange}
              >
                {currencies.map(
                  val =>
                    val.id === parseInt(resultCurrency, 10) ? null : (
                      <option value={val.id} key={val.id}>
                        {val.name}
                      </option>
                    )
                )}
              </Select>
            </Col>
          </Row>
          <Row style={{ alignItems: "center" }}>
            <Col xs={5}>
              <Input
                name="amountInTenge"
                value={Math.round(
                  !amountInTenge && amountInTenge !== 0
                    ? ""
                    : amountInTenge / this.getResultCurrency().price
                )}
                disabled={!resultCurrency}
                onChange={this.handleAmountChange(
                  this.getResultCurrency().price
                )}
              />
            </Col>
            <Col xs={2} />
            <Col xs={5}>
              <Select
                name="resultCurrency"
                value={resultCurrency}
                // onChange={this.handleChange}
              >
                {currencies.map(
                  val =>
                    val.id === parseInt(initialCurrency, 10) ? null : (
                      <option value={val.id} key={val.id}>
                        {val.name}
                      </option>
                    )
                )}
              </Select>
            </Col>
          </Row>
        </Col>
        <Col xs={12} style={{ margin: "2rem 0 0" }}>
          <H1>Курсы валют по сравнению с тенге</H1>
          <Table>
            <tbody>
              {currencies.map(val => (
                <tr key={val.id}>
                  <td>{val.name}</td>
                  <td>{val.price}</td>
                  {val.change > 0 ? (
                    <td style={{ color: "#40b937", opacity: "0.6" }}>
                      {val.change}
                    </td>
                  ) : (
                    <td style={{ color: "#d0021b", opacity: "0.6" }}>
                      {val.change}
                    </td>
                  )}
                  <td>{val.qty}</td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Col>
      </Grid>
    );
  }
}

export default Exchange;
