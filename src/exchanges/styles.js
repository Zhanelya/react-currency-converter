import styled from "styled-components";
// import vars from 'unie-common/vars';

const Wrap = styled.div``;

const Table = styled.table`
  width: ${props => props.width}rem;
  height: ${props => props.height}rem;
  border-radius: 8px;
  background-color: #ffffff;
  margin: 0.5em 0;
  text-align: center;
  width: 100%;
  border-spacing: 0;
  & tbody {
    & tr {
      & td {
        font-size: 1rem;
        text-transform: uppercase;
        padding: 0.875rem 0.5rem;
        &:first-child {
          text-align: left;
          padding: 0.875rem 1.25rem 0.875rem 0;
        }
        &:last-child {
          text-align: right;
          padding: 0.875rem 1.25rem 0.875rem 0.5rem;
        }
      }
    }
  }
`;
const Input = styled.input`
  width: 100%;
  outline: none;
  margin: 0 0.5rem 0 0;
  color: #000;
  font-size: 1rem;
  border-radius: 3px;
  border: solid 1px rgba(105, 78, 159, 0.12);
  padding: 0.7rem 1.25rem;
`;
const Select = styled.select`
  display: block;
  border-radius: 3px;
  width: 100%;
  border: 1px solid rgba(105, 78, 159, 0.12);
  padding: 0.7rem 1.25rem;
  appearance: none;
  background: url("/assets/icons/arrow-down.svg") no-repeat 95% 50% / 24px
    transparent;
  font-size: 1rem;
  color: #000011;
  margin: 0.45rem 0;
`;
const H1 = styled.h1`
  font-size: 1rem;
  font-weight: 600;
  letter-spacing: 0.4px;
  color: #000;
`;

export { Wrap, Select, Table, Input, H1 };
