import React, { Component } from "react";
// import { Query } from 'react-apollo';
// import gql from 'graphql-tag';
// import Loader from 'unie-common/loader';
import Exchange from "./ExchangeItem";
import currencies from "../data/currencies";
// const EX_QUERY = gql`
//   query {
//     currencies: getCurrencies {
//       id
//       name
//       price
//       qty
//       index
//       change
//     }
//   }
// `;

class MainExchange extends Component {
  render() {
    return (
      <Exchange currencies={currencies} />
      // <Query query={EX_QUERY}>
      //   {({ loading, data: { currencies } }) => {
      //     return loading ? <Loader /> : <Exchange currencies={currencies} />;
      //   }}
      // </Query>
    );
  }
}

export default MainExchange;
