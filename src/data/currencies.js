const building = [
  {
    id: 0,
    name: "USD",
    qty: "1",
    price: "369.102",
    change: 0.5
  },
  {
    id: 1,
    name: "RUB",
    qty: "1",
    price: "5.62079",
    change: 0.5
  },
  {
    id: 2,
    name: "EUR",
    qty: "1",
    price: "405",
    change: 0.5
  },
  {
    id: 3,
    name: "GPB",
    qty: "1",
    price: "300",
    change: 0.5
  },
  {
    id: 4,
    name: "CAD",
    qty: "1",
    price: "306",
    change: 0.5
  },
  {
    id: 5,
    name: "AUD",
    qty: "1",
    price: "261",
    change: 0.5
  }
];
export default building;
