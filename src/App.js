import React, { Component } from "react";
import MainExchange from "./exchanges";
import "./App.css";

class App extends Component {
  render() {
    return <MainExchange />;
  }
}

export default App;
